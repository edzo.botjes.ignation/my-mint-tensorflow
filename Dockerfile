FROM vcatechnology/linux-mint

MAINTAINER Edzo Botjes (Ignation) <edzob@ignation.io>
LABEL Description="This image is used to start latest Linux Mint with Apache, PHP, MariaDB, PHPMyadmin, Tensorflow, TensorBox   "

# Linux MINT Docker
## https://github.com/vcatechnology/docker-linux-mint-ci/blob/master/Dockerfile
## https://github.com/vcatechnology/docker-linux-mint
## https://hub.docker.com/r/vcatechnology/linux-mint/

# Linux MINT LAMP+phpmyadmin resources
## http://www.2daygeek.com/install-lamp-stack-apache-mariadb-php-phpmyadmin-on-linuxmint/#
## https://islenmisveri.wordpress.com/2016/09/08/linux-mint-18-x-lamp-linux-apache-mariadb-php-sunucu-kurulumu/
## https://www.digitalocean.com/community/tutorials/how-to-secure-mysql-and-mariadb-databases-in-a-linux-vps

# Tensorflow resources
## https://www.tensorflow.org/install/install_linux#InstallingNativePip
## https://www.tensorflow.org/install/install_sources
### https://docs.bazel.build/versions/master/install-ubuntu.html
### https://www.tensorflow.org/install/install_sources#ConfigureInstallation

#Setting php variables
ENV PHP_INI_FILE='/etc/php/7.0/apache2/php.ini'

#Setting mysql variables
ENV MYSQL_USER='root'
ENV MYSQL_PASSWORD='root'
ENV MYSQL_DATA_DIR='/var/lib/mysql'
ENV MYSQL_RUN_DIR='/run/mysqld'
ENV MYSQL_LOG_DIR='/var/log/mysql'
## Make apt and MySQL happy with the docker environment
RUN echo "#!/bin/sh\nexit 101" >/usr/sbin/policy-rc.d  && \
    chmod +x /usr/sbin/policy-rc.d && \
    groupadd -r mysql && useradd -r -g mysql mysql
	# set installation parameters to prevent the installation script from asking
RUN echo "mysql-server mysql-server/root_password password $MYSQL_PASSWORD" | debconf-set-selections && \
    echo "mysql-server mysql-server/root_password_again password $MYSQL_PASSWORD" | debconf-set-selections
  
####### start install LAMPP
#install Linux-AMPP
RUN apt-get update && apt-get upgrade -yqq

#install Container tools
RUN apt-get install dialog sudo supervisor -yqq
RUN apt-get install vim git git-core curl -yqq

#########################################
#install L-Apache-MPP
RUN apt-get install apache2 -yqq
RUN update-rc.d apache2 defaults
RUN a2enmod rewrite headers expires deflate
RUN sed -i 's|DEFLATE text/html text/plain text/xml|DEFLATE text/html text/plain text/xml text/css text/javascript application/x-javascript|' /etc/apache2/mods-available/deflate.conf

#depedencies for testing apache & php
RUN apt-get install lynx -yqq

#########################################
#install LA-MariaDB-PP
RUN apt-get install mariadb-server mariadb-client -yqq
RUN update-rc.d mysql defaults

RUN mkdir -p $MYSQL_DATA_DIR
RUN usermod -d $MYSQL_DATA_DIR mysql
  
RUN echo "[mysqld]" >> /etc/mysql/my.cnf && \
    echo "bind-address = 127.0.0.1" >> /etc/mysql/my.cnf && \
    echo "datadir = $MYSQL_DATA_DIR" >> /etc/mysql/my.cnf

    RUN sed -i 's/^\(log_error\s.*\)/# \1/' /etc/mysql/my.cnf && \
        echo "mysqld_safe &" > /tmp/config && \
        echo "mysqladmin --silent --wait=10 ping || exit 1" >> /tmp/config && \
        echo "mysql -e 'GRANT ALL PRIVILEGES ON *.* TO \"root\"@\"%\" WITH GRANT OPTION;'" >> /tmp/config && \
        echo "mysql -e 'GRANT ALL ON *.* TO \"$MYSQL_USER\"@\"%\" IDENTIFIED BY \"$MYSQL_PASSWORD\" WITH GRANT OPTION; FLUSH PRIVILEGES;'" >> /tmp/config && \
    bash /tmp/config && \
    rm -f /tmp/config

#########################################
#install LAM-PHP-P
RUN apt-get install php7.0 php7.0-mysql libapache2-mod-php7.0 php7.0-mbstring php7.0-common php7.0-gd php7.0-mcrypt php-gettext php7.0-curl php7.0-cli -yqq
RUN echo -n "<?php phpinfo(); ?>" >> /var/www/html/phpinfo.php

RUN sed -i 's/display_errors = Off/display_errors = On/' $PHP_INI_FILE && \
    sed -i 's/;    display_errors = On/display_errors = On/' $PHP_INI_FILE && \
    echo "extension=uploadprogress.so" >> $PHP_INI_FILE && \
    sed -i 's/memory_limit = 128M/memory_limit = 256M/' $PHP_INI_FILE && \
    sed -i 's/max_execution_time = 30/max_execution_time = 90/' $PHP_INI_FILE && \
    sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 50M/' $PHP_INI_FILE && \
    sed -i 's/post_max_size = 8M/post_max_size = 50M/' $PHP_INI_FILE && \
    sed -i 's/realpath_cache_size = 16k/realpath_cache_size = 1M/' $PHP_INI_FILE && \
    sed -i 's/;realpath_cache_size = 1M/realpath_cache_size = 1M/' $PHP_INI_FILE && \
    sed -i 's/realpath_cache_ttl = 120/realpath_cache_ttl = 3600/' $PHP_INI_FILE && \
    sed -i 's/;realpath_cache_ttl = 3600/realpath_cache_ttl = 3600/' $PHP_INI_FILE
      
#########################################      
#install LAMP-PHPMyadmin
RUN apt-get install php-mysql phpmyadmin -yqq
RUN echo -n "Include /etc/phpmyadmin/apache.conf " >> /etc/apache2/apache2.conf 

#########################################
#########################################
# install TensorFlow from repositories
RUN apt install python3-pip python3-dev -yqq
RUN pip3 install --upgrade pip
RUN pip3 install tensorflow #Python3-CPU non-GPU

# install project specific tensorflow/tensorbox dependecies
RUN pip3 install opencv-python
RUN pip3 install scipy
RUN pip3 install pillow
RUN pip3 install matplotlib
RUN pip3 install PyMySQL

#cleanup
  # Done with apt-get, time to cleanup
RUN apt-get -yqq autoremove && \
    apt-get -yqq autoclean && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*




# Apache - test - bash
## service apache2 start
## service apache2 status
## RUN lynx 127.0.0.1
# MariaDB - test - bash
## service mysql start
## service mysql status
# PHP - test - bash
## service apache2 restart
## service apache2 status
## lynx http://127.0.0.1/phpinfo.php
# PHPMyadmin - test - bash
## apachectl configtest
## service apache2 restart
## service apache2 status
## lynx http://localhost/phpmyadmin

# install TensorFlow via sources
## Get TensorFlowMaster
#RUN git clone https://github.com/tensorflow/tensorflow
## Get Bazel
#RUN apt-get install openjdk-8-jdk -yqq
#RUN echo "deb [arch=amd64] http://storage.googleapis.com/bazel-apt stable jdk1.8" | tee /etc/apt/sources.list.d/bazel.list
#RUN curl https://bazel.build/bazel-release.pub.gpg |  apt-key add -
#RUN apt-get update -yqq
#RUN apt-get install bazel -yqq
## install python dependencies
#RUN apt-get install python3-numpy python3-dev python3-pip python3-wheel -yqq
#RUN apt-get install libcupti-dev -yqq
#RUN cd tensorflow && ./configure
# ./configure --help
#RUN cd tensorflow && bazel build --config=opt //tensorflow/tools/pip_package:build_pip_package
#RUN pip install /tmp/tensorflow_pkg/tensorflow-1.3.0-py2-none-any.whl


